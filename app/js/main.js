$(document).ready(function() {

	$(".nav-trigger").click(function () {
      $(".nav-list").slideToggle("fast");
   	});

	$('#tabs').tabs();
	$('#light-cars').tabs();
	$('#weight-cars').tabs();
	$('#delievery-tabs').tabs();
	$('.feed-slider').slick();
	$('.clients-slider').slick({
		slidesToShow: 4,
		slidesToScroll: 4
	});
	$('#shop-slider').owlCarousel({
      nav : false,
      dots : true,
      items : 1
	});

	$('.adv-list__item').hover(
		function(){
			var textWrap = $(this).children('.adv-list-item__text');
				text = textWrap.html();
				containerText = $('.adv-list__text-wrap');

			containerText.html(text);
			containerText.css('visibility','visible');
		},
		
		function(){
			
			containerText.children().remove();
			containerText.css('visibility','hidden');
		}
	);
});

